# Test plan for changes in the website

## Unit testing

* Network timeout error management - Both on page load and on Post request output
* Access Exceptions - 4XX error management
* Quality of Service (QoS) - Retry later (up to a reasonable level) if failed to connect
* Different page layout between one way and return flights
* Manage if flight operator is different to the airline ticket provider

### Unexpected input types to functions - Edge Cases
* Invalid data content
* Empty strings
* Null values
* Different datetime string formats
* Content parsing exception management. Different flight itineries may have slight variations
* Filter out special characters
* Replace characters that can affect file (JSON, CSV) file formatting such as eg. , ' " [ ] { }

## Integration testing

* Test file inputs not up to standard, missing columns for example
* Install / uninstall test - deployment
* Test javascript code across different browsers

## Performance Testing

* Data throughput volume - Rate limiting
* Scale testing - Improve Big(O) performance
* Caching
* Implement search on data lookups rather than brute force search
* Implement a cap on the maximum input string size limits

## Internationalisation

* Language translation
* Discover how country affects layout. Different cultures affects UI. Likely to have different version by region. If so will impact the logic of the website parser.
