# TODO

* Convert into typed values for JSON output in javascript
* Pull in time zones and convert into unix time stamps
* Can you do anything with that flight number?
* Weather lookup by location from 3rd party weather provider. eg. Accuweather
* Have a running REST server side receiving the data using Flask. Have it parse received JSOn into CSV files
* Decompose parsing functions to make easier to test
