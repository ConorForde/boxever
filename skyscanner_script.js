function generateUUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function undefinedToNull(input) {
	if ((typeof(oldins) !== "undefined") && (oldins === ins)) {
		return null;
	}
	return input;
}

function transmitData(json_data) {
	// Note: The following must be run on a proxy server. Running in browser console triggers CORS security policy
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "http://api.capturedata.ie", true);
	xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.withCredentials = true;
	xhr.send(JSON.stringify(json_data));
}

function parsePage(html_input){
	var list = html_input.getElementById("details-container").getElementsByTagName("section")[0].childNodes;

	if(list === undefined) {
		return; // Break early, nothing in the web page matched the expected format
	}

	var data_list = {};
	data_list.flights = [];

	// iterate over elements and output their HTML content
	for (var i=0; i<list.length; i++){
		var airline = undefinedToNull(list[i].childNodes[2].getElementsByTagName("img")[0].title);
		var flight_id = list[i].childNodes[2].childNodes[0].getElementsByTagName("span")[0].innerHTML;
		flight_id = undefinedToNull(flight_id.replaceAll(" ", "").replaceAll("react-text", "").replaceAll("/", "").replaceAll("-", "").split("<!>", 3)[2].split(">", 2)[1]);

		var departure = list[i].childNodes[2].childNodes[0].childNodes[1].childNodes[1].childNodes[2].childNodes[0].innerHTML;
		var departure_id = undefinedToNull(departure.split(" ", 2)[0]);

		// After the departure ID has been extracted...
		departure = undefinedToNull(departure.substring(departure.indexOf(" ") + 1, departure.length));

		var arrival = list[i].childNodes[2].childNodes[0].childNodes[1].childNodes[1].childNodes[2].childNodes[1].innerHTML;
		var arrival_id = undefinedToNull(arrival.split(" ", 2)[0])

		// After the arrival ID has been extracted...
		arrival = undefinedToNull(arrival.substring(arrival.indexOf(" ") + 1, arrival.length))

		var duration = undefinedToNull(list[i].childNodes[2].childNodes[0].childNodes[1].childNodes[1].childNodes[1].childNodes[1].innerHTML);
		
		var departure_time = list[i].childNodes[2].childNodes[0].childNodes[1].childNodes[1].childNodes[1].childNodes[0].innerHTML;
		departure_time = undefinedToNull(departure_time.split("-->")[1].split("<!--")[0]);

		var arrival_time = list[i].childNodes[2].childNodes[0].childNodes[1].childNodes[1].childNodes[1].childNodes[2].innerHTML;
		arrival_time = undefinedToNull(arrival_time.split("-->")[1].split("<!--")[0]);

		var uuid = generateUUID()

		var data = {
			"uuid": uuid,
			"airline": airline,
			"operator": airline,
			"flight_number": flight_id,
			"departure": departure,
			"departure_id": departure_id,
			"destination": arrival,
			"destination_id": arrival_id,
			"departure_time": departure_time,
			"departure_timezone": null,
			"arrival_time": arrival_time,
			"arrival_timezone": null,
			"duration": duration,
			"price": null,
			"currency": null
		};

		var csv_data = uuid + "," + airline + "," + airline + "," + flight_id + "," + departure_id;
		console.log(csv_data);
		data_list.flights.push(data);
	}

	console.log(JSON.stringify(data_list));
	transmitData(data_list)
}

parsePage(document);
