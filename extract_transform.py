# coding: utf-8

from time import sleep
from json import dumps
from csv import reader
from requests import post
from dataclasses import dataclass
from requests.exceptions import ConnectionError
import schedule

# Data class syntax introduced in Python 3.7
@dataclass
class Location:
    latitude: float = None
    longitude: float = None

# cache the airport id lookup

def get_location(airport_id):
    """
    Given an airport id, checks against a database of locations to get its latitude & longitude.
    Data source: https://github.com/datasets/airport-codes
    """
    with open('airports.csv', 'r') as csvfile:
        airport_list = reader(csvfile, delimiter=',')

        for row in airport_list:
            #if current rows 2nd value is equal to input, print that row
            if airport_id == row[1]:
                return Location(row[4], row[5]) # TODO type checking
    
    return Location() 

        # TODO get list where airport id matches
        # TODO caching of look up as this is pretty intensive

def empty_check(input_value):
    """Checks if the input variable is a known empty string. If so converts to a None type"""
    if input_value == "" or input_value == "":
        return None

    return input_value

def part_2(testing=False):
    """Reads in data from a CSV file,
    parses each line and sends to a rest endpoint with a JSON payload"""
    # Read in the data from a CSV file
    with open('input_data.csv', 'r') as csvfile:
        data_reader = reader(csvfile, delimiter=',')

        json_data = {}
        json_data['flights'] = []

        for i,row in enumerate(data_reader):
            for row in data_reader: # For each line...
                # Parse
                
                departure_location = get_location(empty_check(row[5]))
                destination_location = get_location(empty_check(row[7]))
                
                # TODO use the locations to look up relevant weather data

                json_data['flights'].append({
                    'uuid': empty_check(row[0]),
                    'airline': empty_check(row[1]),
                    'operator': empty_check(row[2]),
                    'flight_number': empty_check(row[3]),
                    'departure': empty_check(row[4]),
                    'departure_id': empty_check(row[5]),
                    'destination': empty_check(row[6]),
                    'destination_id': empty_check(row[7]),
                    'departure_time': empty_check(row[8]),
                    'arrival_time': empty_check(row[9]),
                    'duration': empty_check(row[10])
                })            
            if(i >= 9):
                break # up to 10 lines only
        
    # TODO rate limiting / max value

    # send all the data to a json file when testing
    test_file = open("test.json", "w")
    test_file.write(dumps(json_data, indent=4, separators=(',', ': ')))

    url = "http://api.capturedata.ie"

    # Try sending data to REST endpoint
    try:
        response = post(url, data=json_data)
        if testing or response.status_code != 200:
            print(response.status_code, response.reason)
    except ConnectionError as ex:
        if testing:
            print("Failed to connect to " + url + ". Did not send data:")
            print(json_data)
        else:
            print("Failed to connect to " + url)
    except Exception as ex:
        print(str(ex))

def repeating_job():
    part_2(True)

if __name__ == "__main__":
    schedule.every(10).seconds.do(repeating_job)

    while True:
        schedule.run_pending()
        sleep(1)
